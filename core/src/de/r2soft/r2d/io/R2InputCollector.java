package de.r2soft.r2d.io;

import com.badlogic.gdx.InputProcessor;

/**
 * Master input processor to manage input for an entire application all in one class with simple input injectors to work
 * with.
 * 
 * @author Katharina Sabel <katharina.sabel@2rsoftworks.de>
 *
 */
public class R2InputCollector implements InputProcessor {

  public boolean keyDown(int keycode) {
	return false;
  }

  public boolean keyUp(int keycode) {
	return false;
  }

  public boolean keyTyped(char character) {
	return false;
  }

  public boolean touchDown(int screenX, int screenY, int pointer, int button) {
	return false;
  }

  public boolean touchUp(int screenX, int screenY, int pointer, int button) {
	return false;
  }

  public boolean touchDragged(int screenX, int screenY, int pointer) {
	return false;
  }

  public boolean mouseMoved(int screenX, int screenY) {
	return false;
  }

  public boolean scrolled(int amount) {
	// TODO Auto-generated method stub
	return false;
  }

}
