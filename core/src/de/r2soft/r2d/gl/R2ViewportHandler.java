/* #########################################################################
 * Copyright (c) 2014 RANDOM ROBOT SOFTWORKS
 * (See @authors file)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ######################################################################### */

package de.r2soft.r2d.gl;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;

/**
 * The master viewport handler to handle ALL R2D viewport elements hard-coded with utility functions into R2D. Is
 * attached to the R2Core and will render every frame for the entire application (after everything else was rendered to
 * put it on top of the pipeline stack.
 * 
 * @author Katharina Sabel <katharina.sabel@2rsoftworks.de>
 */
public class R2ViewportHandler {
  private Stage stage; // TODO: Change to R2Stage

  public R2ViewportHandler(float width, float height) {
	stage = new Stage(new StretchViewport(width, height));
  }
}
