/* #########################################################################
 * Copyright (c) 2014 RANDOM ROBOT SOFTWORKS
 * (See @authors file)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ######################################################################### */

package de.r2soft.r2d.exceptions;

/**
 * An exception to be thrown if ANY Master --> Slave binding (or chaining...hehehe) in R2D fails. Will carry a
 * bidirectional pointer between the Master-Slave-Instances in question as well as a {@link R2Context} variable
 * 
 * @author Katharina Sabel <katharina.sabel@2rsoftworks.de>
 */
public class R2DSlaveChainFailureException extends Exception {
  private static final long serialVersionUID = 8961893734253720198L;

  public R2DSlaveChainFailureException() {
	super();
  }

}
