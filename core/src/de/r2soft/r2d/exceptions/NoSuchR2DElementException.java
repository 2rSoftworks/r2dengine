/* #########################################################################
 * Copyright (c) 2014 RANDOM ROBOT SOFTWORKS
 * (See @authors file)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ######################################################################### */

package de.r2soft.r2d.exceptions;

/**
 * An exception that is thrown whenever the user tries to call or update an R2D viewport element that has not previously
 * been added to the viewport!
 * 
 * @author Katharina Sabel <katharina.sabel@2rsoftworks.de>
 */
public class NoSuchR2DElementException extends Exception {
  private static final long serialVersionUID = -8348575693365844268L;

  public NoSuchR2DElementException() {
	super();
  }

  public NoSuchR2DElementException(String message) {
	super(message);
  }

  public NoSuchR2DElementException(String message, Throwable cause) {
	super(message, cause);
  }

}
