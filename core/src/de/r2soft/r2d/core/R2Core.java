/* #########################################################################
 * Copyright (c) 2014 RANDOM ROBOT SOFTWORKS
 * (See @authors file)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ######################################################################### */
package de.r2soft.r2d.core;

import java.util.Stack;
import java.util.logging.Logger;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import de.r2soft.r2d.gl.R2Overlay;
import de.r2soft.r2d.gl.R2Screen;
import de.r2soft.r2d.gl.R2ViewportHandler;
import de.r2soft.r2d.types.R2Version;

public class R2Core implements ApplicationListener {
  private R2ViewportHandler viewportHandler = new R2ViewportHandler(0f, 0f);

  /** Master Variables are accessible by the ENTIRE application */
  public R2Version MASTER_VERSION;
  private Stage MASTER_STAGE;

  private Label fps;
  private SpriteBatch batch;
  private R2Screen screen;
  private Stack<R2Overlay> overlays;
  private TextureRegion background;
  private Logger log = Logger.getLogger(getClass().getSimpleName());

  /** Init a game core with a version */
  public R2Core(R2Version version) {
	overlays = new Stack<R2Overlay>();
	this.MASTER_VERSION = version;
  }

  public R2Core() {
	this(null);
  }

  public void create() {
	batch = new SpriteBatch();
	MASTER_STAGE = new Stage(new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
	Skin MASTERSKIN = new Skin(Gdx.files.internal("uiskin.json"));
	fps = new Label("FPS: ", MASTERSKIN);
	Table tmp = new Table();
	tmp.setFillParent(true);
	tmp.top().left();
	tmp.add(fps);
	MASTER_STAGE.addActor(tmp);
  }

  public void resize(int width, int height) {
	if (screen != null)
	  screen.resize(width, height);

	if (!overlays.isEmpty())
	  for (R2Overlay o : overlays)
		o.resize(width, height);
  }

  public void render() {
	Gdx.gl.glClearColor(0, 0, 0, 1);
	Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

	float intFPS = Gdx.graphics.getFramesPerSecond();
	if (fps != null)
	  fps.setText("FPS: " + intFPS);

	batch.begin();
	if (background != null)
	  batch.draw(background, 0, 0, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 1, 1, 0);
	batch.end();

	if (screen != null)
	  screen.render(Gdx.graphics.getDeltaTime());

	if (!overlays.isEmpty())
	  for (R2Overlay o : overlays)
		o.render(Gdx.graphics.getDeltaTime());

	R2InputManifold.getInstance(this).updateSlave();

	if (MASTER_STAGE != null)
	  MASTER_STAGE.draw();
  }

  public void pause() {
	if (screen != null)
	  screen.pause();
  }

  public void resume() {
	if (screen != null)
	  screen.resume();
  }

  public void dispose() {
	if (screen != null)
	  screen.dispose();
	if (!overlays.isEmpty())
	  for (R2Overlay o : overlays)
		o.dispose();
  }

  public void setScreen(R2Screen screen) {
	if (this.screen != null)
	  this.screen.hide();
	this.screen = screen;
	if (this.screen != null) {
	  this.screen.show();
	  this.screen.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	}
  }

  public R2Screen getScreen() {
	return screen;
  }

  public void addOverlay(R2Overlay overlay) {
	overlays.add(overlay);
	overlay.show();
	overlay.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
  }

  /**
   * Try to avoid use of this method as it can remove an overlay from the middle of a stack. Use
   * {@link #removeOverlay(boolean)} instead!
   */
  @Deprecated
  public void removeOverlay(R2Overlay overlay) {
	if (overlays.contains(overlay)) {
	  overlays.remove(overlay);
	  overlay.dispose();
	}
	else
	  log.info("Overlay wasn't found in Collection");
  }

  /** Pops the last overlay from the Stack */
  public void removeOverlay() {
	if (!overlays.isEmpty())
	  overlays.pop().dispose();
	else
	  log.info("No overlays were found in Collection");
  }

  /**
   * Avoid using this unless you need to iterate over all the overlays. Use {@link #peekOverlays()} instead!
   */
  public Stack<R2Overlay> getOverlays() {
	return overlays;
  }

  /** Returns last item of stack */
  public R2Overlay peekOverlays() {
	return overlays.peek();
  }

  /**
   * Returns if the Overlays Stack is empty to avoid fetching the entire object to another class
   */
  public boolean isOverlaysEmpty() {
	return overlays.isEmpty();
  }

}