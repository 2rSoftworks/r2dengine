/* #########################################################################
 * Copyright (c) 2014 RANDOM ROBOT SOFTWORKS
 * (See @authors file)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ######################################################################### */
package de.r2soft.r2d.core;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Audio;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.LifecycleListener;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Clipboard;

/**
 * Main entry point for an OpenGL window application using a 2D viewport.
 * 
 * @author Katharina Sabel <katharina.sabel@2rsoftworks.de>
 *
 */
class R2DWindowApplication implements Application {
  private R2CoreConfigurator config;
  private R2Core core;

  public R2DWindowApplication(R2CoreConfigurator config, R2Core core) {
	this.config = config;
	this.core = core;
  }

  public ApplicationListener getApplicationListener() {
	return null;
  }

  public Graphics getGraphics() {
	return null;
  }

  public Audio getAudio() {
	return null;
  }

  public Input getInput() {
	return null;
  }

  public Files getFiles() {
	return null;
  }

  public Net getNet() {
	return null;
  }

  public void log(String tag, String message) {

  }

  public void log(String tag, String message, Throwable exception) {

  }

  public void error(String tag, String message) {

  }

  public void error(String tag, String message, Throwable exception) {

  }

  public void debug(String tag, String message) {

  }

  public void debug(String tag, String message, Throwable exception) {

  }

  public void setLogLevel(int logLevel) {

  }

  public int getLogLevel() {
	return 0;
  }

  public ApplicationType getType() {
	return null;
  }

  public int getVersion() {
	return 0;
  }

  public long getJavaHeap() {
	return 0;
  }

  public long getNativeHeap() {
	return 0;
  }

  public Preferences getPreferences(String name) {
	return null;
  }

  public Clipboard getClipboard() {
	return null;
  }

  public void postRunnable(Runnable runnable) {

  }

  public void exit() {

  }

  public void addLifecycleListener(LifecycleListener listener) {

  }

  public void removeLifecycleListener(LifecycleListener listener) {

  }

}