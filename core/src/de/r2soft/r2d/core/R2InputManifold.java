/* #########################################################################
 * Copyright (c) 2014 RANDOM ROBOT SOFTWORKS
 * (See @authors file)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ######################################################################### */
package de.r2soft.r2d.core;

import de.r2soft.r2d.gl.R2Screen;

/**
 * 
 * @author: Katharina Sabel <katharina.sabel@2rsoftworks.de>
 */
public class R2InputManifold {
  private static R2InputManifold matrix = null;
  private static R2Core master;
  private R2Screen slave;
  private R2Screen last;

  private R2InputManifold() {

  }

  public static R2InputManifold getInstance(R2Core master) {
	if (matrix == null) {
	  matrix = new R2InputManifold();
	  R2InputManifold.master = master;
	}
	return matrix;
  }

  public void updateSlave() {
	if (!master.isOverlaysEmpty()) {
	  if (last == null || !last.equals(master.peekOverlays()))
		master.peekOverlays().setInputFocus();
	  last = master.peekOverlays();
	}
	else {
	  if (last == null || !last.equals(master.getScreen())) {
		master.getScreen().setInputFocus();
		last = master.getScreen();
	  }
	}
  }

  public void setSlave(R2Screen slave) {
	this.slave = slave;
  }

  public R2Screen getSlave() {
	return slave;
  }
}
