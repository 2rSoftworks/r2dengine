/* #########################################################################
 * Copyright (c) 2014 RANDOM ROBOT SOFTWORKS
 * (See @authors file)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ######################################################################### */

package de.r2soft.r2d.db;

import java.util.ArrayList;
import java.util.List;

/**
 * A helper class to store user keys for db I/O and dynamically generate new ones.
 * 
 * @author Katharina Sabel <katharina.sabel@2rsoftworks.de>
 */
public class R2Keys {

  private List<String> keys;

  public R2Keys() {
	keys = new ArrayList<String>();
  }
}
