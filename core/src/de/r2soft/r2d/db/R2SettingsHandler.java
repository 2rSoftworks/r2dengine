/* #########################################################################
 * Copyright (c) 2014 RANDOM ROBOT SOFTWORKS
 * (See @authors file)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ######################################################################### */

package de.r2soft.r2d.db;

/**
 * Backend settings helper that can be extended to create settings files for games with uniform key-names.
 * 
 * @author Katharina Sabel <katharina.sabel@2rsoftworks.de>
 */
public class R2SettingsHandler {
  private String[] keys;
  private String masterKey;

  public R2SettingsHandler(String gameName, Object mainClass) {
	this.generateMasterKey(gameName, mainClass.getClass().getPackage());
  }

  /** Generates a db key with a game name and package. */
  private void generateMasterKey(String gameName, Package packageName) {
	String repr = packageName.getName().replace(".", "_");
	
	
	int end = repr.indexOf("_", repr.indexOf("_") + 1);
	repr = repr.substring(0, end);
	System.out.println(repr);
  }
}
