/* #########################################################################
 * Copyright (c) 2014 RANDOM ROBOT SOFTWORKS
 * (See @authors file)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ######################################################################### */

package de.r2soft.r2d.utils;

/**
 * A collection of string utilities to use in your application.
 * 
 * @author Katharina Sabel <katharina.sabel@2rsoftworks.de>
 */
public class R2StrUtils {

  /**
   * Returns the index within this string of the n-th occurrence of the specified substring.
   * <p>
   * The returned index is the smallest value <i>k</i> for which: <blockquote>
   * 
   * <pre>
   * this.startsWith(str, <i>k</i>)
   * </pre>
   * 
   * </blockquote> If no such value of <i>k</i> exists, then {@code -1} is returned.
   * 
   * @param string
   *          the string to search in
   * @param subString
   *          the string to search for
   * @param n
   *          the occurrence of the substring that will be returned.
   * 
   * @return the nth occurrence of a substring in a global string.
   * @author Katharina Sabel
   */
  public static int nthIndexOf(String string, String subString, int n) {
	int idx = string.indexOf(subString);
	if (idx == -1)
	  return -1;
	for (int c = 1; c < n; c++) {
	  idx = string.indexOf(subString, idx + 1);
	  if (idx == -1)
		return -1;
	}
	return idx;
  }
}
