/* #########################################################################
 * Copyright (c) 2014 RANDOM ROBOT SOFTWORKS
 * (See @authors file)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ######################################################################### */

package de.r2soft.r2d.utils;

import javafx.util.Pair;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;

import de.r2soft.r2d.exceptions.NoSuchR2DElementException;
import de.r2soft.r2d.values.R2Assets;

/**
 * A collection viewport and rendering utilities. Include but not limited to FPS counters, pre-setup table layouts,
 * simple menu rendering and logo/ build-number handling on screen. Most methods will require a stage to be passed into
 * them.
 * 
 * @author Katharina Sabel <katharina.sabel@2rsoftworks.de>
 */
public class ViewportUtils {

  /**
   * Adds an FPS counter to a particular corner of the screen. Uses DEFAULT skin that can be overwritten with
   * {@link #overrideDefaultSkin(Skin)}. The <code>Pair<Align, Align></code> specifies the corner of the screen that
   * should be used. DON'T FORGET TO CALL {@link ViewportUtils.updateFPSCounter()} every frame!
   * <p>
   * The first parameter is either {@link Align.left} or {@link Align.right}
   * 
   * <p>
   * The first parameter is either {@link Align.top} or {@link Align.bottom}
   * 
   * @param stage
   *          Containing stage of a screen
   * @param alignment
   *          Specifies the corner of the screen that should be used.
   * 
   * @return The manipulated {@link #Stage} of function chaining
   */
  public static Stage addFPSCounter(Stage stage, Pair<Align, Align> alignment) {
	Table fpsContainer = new Table();
	fpsContainer.setFillParent(true);
	if (alignment.getKey().equals(Align.left))
	  fpsContainer.left();
	else
	  fpsContainer.right();
	if (alignment.getValue().equals(Align.top))
	  fpsContainer.top();
	else
	  fpsContainer.bottom();
	fpsContainer.add(new Label("FPS: NaN", R2Assets.R2Skin));
	stage.addActor(fpsContainer);
	return stage;
  }

  /**
   * Updates the FPS values on the previously added FPS Counter. Will throw a {@link #NoSuchR2DElementException} if
   * FPSCounter wasn't previously added to a stage.
   */
  public void updateFPSCounter() throws NoSuchR2DElementException {
	
  }

  /**
   * Overrides the default skin for the basic renderings
   * 
   * @param skin
   */
  public static void overrideDefaultSkin(Skin skin) {

  }

}
