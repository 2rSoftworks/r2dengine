/* #########################################################################
 * Copyright (c) 2014 RANDOM ROBOT SOFTWORKS
 * (See @authors file)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0
 * 
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ######################################################################### */

package de.r2soft.r2d.types;

/**
 * A version identified for a game. Can be compared, hashed and printed out.
 * 
 * @author Katharina Sabel <katharina.sabel@2rsoftworks.de>
 */
public class R2Version {

  /** Avoid changing these values. Use setters instead! */
  public int MAJOR = -1;
  /** Avoid changing these values. Use setters instead! */
  public int MINOR = -1;
  /** Avoid changing these values. Use setters instead! */
  public int BUILD = -1;

  public R2Version(int mayor, int minor, int build) {
	this.MAJOR = mayor;
	this.MINOR = minor;
	this.BUILD = build;
  }

  /** Changes the Major number of an application, setting the Minor to 0 and build to 1 */
  public void changeMajor(int newMajor) {
	this.MAJOR = newMajor;
	this.changeMinor(0);
  }

  /** Changing the minor number of an application, changing the build number back to 1. */
  public void changeMinor(int newMinor) {
	this.MINOR = newMinor;
	this.BUILD = 1;
  }

  /** Increments the build number for automatic builds. */
  public void incrementBuild() {
	System.out.println("Changing build number from " + BUILD++ + " to " + BUILD);
  }

  @Override
  public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append(MAJOR);
	sb.append(".");
	sb.append(MINOR);
	sb.append(".");
	sb.append(BUILD);
	return sb.toString();
  }

  @Override
  public boolean equals(Object o) {
	if (o instanceof R2Version) {
	  if (this.MAJOR == ((R2Version) o).MAJOR && this.MINOR == ((R2Version) o).MINOR
		  && this.BUILD == ((R2Version) o).BUILD) {
		return true;
	  }
	}
	return false;
  }
}
